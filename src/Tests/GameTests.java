package Tests;

import Enums.Difficulty;
import Enums.Rank;
import Enums.Suit;
import Game.Game;
import Models.Card;
import Models.ComputerPlayer;
import org.junit.Test;
import static org.junit.Assert.*;

public class GameTests {

    @Test
    public void createComputerPlayerTest(){
        Game game = new Game();
        ComputerPlayer computerPlayer = new ComputerPlayer("TestName", Difficulty.EASY);
        ComputerPlayer testPlayer = game.createComputerPlayer("TestName", Difficulty.EASY);
        assertEquals(computerPlayer.getName(), testPlayer.getName());
        assertEquals(computerPlayer.getDifficulty(), testPlayer.getDifficulty());
    }

    @Test
    public void checkIfCurrentPlayerWonTest(){
        Game game = new Game();
        ComputerPlayer computerPlayer = new ComputerPlayer("TestName", Difficulty.EASY);
        assertTrue(game.playerWon(computerPlayer));
    }

    @Test
    public void checkIfCurrentPlayerWonTestFail(){
        Game game = new Game();
        ComputerPlayer computerPlayer = new ComputerPlayer("TestName", Difficulty.EASY);
        computerPlayer.addCard(new Card(Suit.CLUBS, Rank.SEVEN));
        assertTrue(!game.playerWon(computerPlayer));
    }
}
