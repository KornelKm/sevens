package Tests;

import Enums.Suit;
import Models.Card;
import ServicesImpl.DeckServiceImpl;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class DeckServiceTests {

    @Test
    public void deckSizeTest() {
        DeckServiceImpl deckService = new DeckServiceImpl();
        assertEquals(52, deckService.generateNewDeck().size());
    }

    @Test
    public void generateCardsOnlyForGivenSuitTest() {
        DeckServiceImpl deckService = new DeckServiceImpl();
        Suit testedSuit = Suit.CLUBS;
        ArrayList<Card> cards = deckService.generateCardsForSuit(testedSuit);
        for (Card card:cards) {
            assertEquals(testedSuit, card.getSuit());
        }

    }
}
