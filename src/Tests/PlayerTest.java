package Tests;
import Enums.Rank;
import Enums.Suit;
import Models.Card;
import Models.UserPlayer;
import org.junit.Test;

import static org.junit.Assert.*;
public class PlayerTest {

    @Test
    public void hasSevenFailTest() {
        UserPlayer player = new UserPlayer("Testname");
        assertTrue(!player.hasAnySeven());
    }

    @Test
    public void hasSevenTest() {
        UserPlayer player = new UserPlayer("Testname");
        player.addCard(new Card(Suit.CLUBS, Rank.SEVEN));
        assertTrue(player.hasAnySeven());
    }
}
