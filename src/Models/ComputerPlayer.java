package Models;

import Enums.Difficulty;
import Enums.Suit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class ComputerPlayer extends Player {
    private Difficulty difficulty;

    public ComputerPlayer(String name) {
        super(name);
    }

    public ComputerPlayer(String name, Difficulty difficulty) {
        super(name);
        this.difficulty = difficulty;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    @Override
    public PlayerMove getNextPlayerMove(Player player, Card[][] board) {
        ComputerPlayer computerPlayer = (ComputerPlayer) player;
        if (computerPlayer.getDifficulty() == Difficulty.EASY) {
            return easyMove(computerPlayer, board);
        } else {
            return hardMove(computerPlayer, board);
        }
    }
    PlayerMove easyMove(ComputerPlayer player, Card[][] board) {
        if (player.hasAnySeven()) {
            return putSeven(player.getSeven(), board);
        } else {
            for (int i = 0; i < 4; i++) {
                ArrayList<Card> cardsWithSuit = getPlayerCardsWithGivenSuit(Suit.getSuitByValue(i), player);
                if (cardsWithSuit.size() > 0) {
                    for (int j = 0; j < 13; j++) {
                        if (board[i][j] != null) {
                            if (hasNeighbourCard(board[i][j].getRank().getValue(), cardsWithSuit)) {
                                return getPossibleCardMove(board[i][j].getRank().getValue(), i, j, cardsWithSuit);
                            }
                        }

                    }
                }
            }
        }
        return new PlayerMove(true);
    }

    PlayerMove putSeven(Card card, Card[][] board) {
        return new PlayerMove(card.getSuit().getValue(), 5, card, false);
    }

    boolean hasNeighbourCard(int rank, ArrayList<Card> cardsWithSuit) {
        for (Card card : cardsWithSuit) {
            if (card.getRank().getValue() > rank) {
                if (card.getRank().getValue() - rank == 1) {
                    return true;
                }
            } else {
                if (rank - card.getRank().getValue() == 1) {
                    return true;
                }
            }

        }
        return false;
    }

    PlayerMove getPossibleCardMove(int rank, int cardCol, int cardRow, ArrayList<Card> cardsWithSuit) {
        for (Card card : cardsWithSuit) {
            if (card.getRank().getValue() > rank) {
                if (card.getRank().getValue() - rank == 1) {
                    return new PlayerMove(cardCol, cardRow + 1, card, false);
                }
            } else {
                if (rank - card.getRank().getValue() == 1) {
                    return new PlayerMove(cardCol, cardRow - 1, card, false);
                }
            }

        }
        return new PlayerMove(true);
    }

    PlayerMove hardMove(ComputerPlayer player, Card[][] board) {
        HashMap<Suit, Integer> suitsWithCardsAmount = getSuitsWithCardsAmount(player);

        while (suitsWithCardsAmount.size() > 0) {
            Suit suit = getSuitWithTheMostCards(suitsWithCardsAmount);
            ArrayList<Card> userCardsInSuit = getPlayerCardsWithGivenSuit(Suit.getSuitByValue(suit.getValue()), player);
            if (userCardsInSuit.size() > 0) {
                for (int j = 0; j < 13; j++) {
                    if (board[suit.getValue()][j] != null) {
                        if (hasNeighbourCard(board[suit.getValue()][j].getRank().getValue(), userCardsInSuit)) {
                            return getPossibleCardMove(board[suit.getValue()][j].getRank().getValue(), suit.getValue(), j, userCardsInSuit);
                        }
                    }

                }
            }
            suitsWithCardsAmount.remove(suit);
        }

        if (player.hasAnySeven()) {
            return putSeven(player.getSeven(), board);
        }

        return new PlayerMove(true);
    }

    public Suit getSuitWithTheMostCards(HashMap<Suit, Integer> cardsInSuit) {
        return Collections.max(cardsInSuit.entrySet(), (entry1, entry2) -> entry1.getValue() - entry2.getValue()).getKey();
    }

    HashMap<Suit, Integer> getSuitsWithCardsAmount(ComputerPlayer player) {
        int clubs = 0;
        int diamonds = 0;
        int hearts = 0;
        int spades = 0;
        HashMap<Suit, Integer> cardsInSuit = new HashMap<>();
        for (Card card : player.getCards()) {
            switch (card.getSuit()) {
                case CLUBS:
                    clubs++;
                    break;
                case DIAMONDS:
                    diamonds++;
                    break;
                case HEARTS:
                    hearts++;
                    break;
                case SPADES:
                    spades++;
                    break;
            }
        }
        cardsInSuit.put(Suit.CLUBS, clubs);
        cardsInSuit.put(Suit.DIAMONDS, diamonds);
        cardsInSuit.put(Suit.HEARTS, hearts);
        cardsInSuit.put(Suit.SPADES, spades);
        return cardsInSuit;
    }

    ArrayList<Card> getPlayerCardsWithGivenSuit(Suit suit, ComputerPlayer player) {
        ArrayList<Card> cardsWithSuit = new ArrayList<>();
        for (Card card : player.getCards()) {
            if (card.getSuit() == suit) {
                cardsWithSuit.add(card);
            }
        }
        return cardsWithSuit;
    }

}
