package Models;

import Enums.Rank;
import Enums.Suit;
import Services.PlayerService;

import java.util.ArrayList;

public class Player implements PlayerService {

    Player(String name) {
        this.name = name;
    }

    private String name;
    private ArrayList<Card> cards = new ArrayList<>();

    public ArrayList<Card> getCards() {
        return cards;
    }

    public void setCards(ArrayList<Card> cards) {
        this.cards = cards;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addCard(Card card) {
        this.cards.add(card);
    }

    public boolean hasSevenOfClubs() {
        for (Card card : getCards()) {
            if (card.getRank() == Rank.SEVEN && card.getSuit() == Suit.CLUBS) {
                return true;
            }

        }
        return false;
    }

    public boolean hasAnySeven() {
        for (Card card : getCards()) {
            if (card.getRank() == Rank.SEVEN) {
                return true;
            }

        }
        return false;
    }

    @Override
    public Card getSeven() {
        for (Card card : getCards()) {
            if (card.getRank() == Rank.SEVEN) {
                return card;
            }

        }
        return null;
    }


    @Override
    public PlayerMove getNextPlayerMove(Player player, Card[][] board) {
        return null;
    }
}
