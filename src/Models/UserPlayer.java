package Models;

import Enums.Rank;
import Services.PlayerService;

import java.util.Scanner;

public class UserPlayer extends Player {

    public UserPlayer(String name) {
        super(name);
    }

    @Override
    public PlayerMove getNextPlayerMove(Player player, Card[][] board) {
        Card card = pickCardFromUser(player);
        if (card == null) {
            return new PlayerMove(true);
        }
        if (canPutChosenCard(card, board)) {
            return getPlayerMove(card, board);
        } else {
            return getNextPlayerMove(player, board);
        }
    }

    Card pickCardFromUser(Player player) {
        System.out.println("Type pass to skip move or Type card name from:");
        printCardsInHand(player);
        Scanner scanner = new Scanner(System.in);
        String chosenCardName = scanner.nextLine();
        if (chosenCardName.equals("pass")) {
            return null;
        }
        for (Card card : player.getCards()) {
            if (card.toString().equals(chosenCardName)) {
                return card;
            }
        }
        System.out.println("User do not have such card");
        return pickCardFromUser(player);
    }

    boolean canPutChosenCard(Card card, Card[][] board) {
        if (card.getRank() == Rank.SEVEN) {
            return true;
        }
        int cardSuit = card.getSuit().getValue();
        int rank = card.getRank().getValue();

        for (int i = 0; i < 13; i++) {
            if (board[cardSuit][i] != null) {
                if (board[cardSuit][i].getRank().getValue() > rank) {
                    if (board[cardSuit][i].getRank().getValue() - rank == 1) {
                        return true;
                    }
                } else {
                    if (rank - board[cardSuit][i].getRank().getValue() == 1) {
                        return true;
                    }
                }
            }

        }
        return false;
    }

    PlayerMove getPlayerMove(Card card, Card[][] board) {

        int cardSuit = card.getSuit().getValue();
        int rank = card.getRank().getValue();
        if (card.getRank() == Rank.SEVEN) {
            return new PlayerMove(cardSuit, 5, card, false);
        }
        for (int i = 0; i < 13; i++) {
            if (board[cardSuit][i] != null) {
                if (board[cardSuit][i].getRank().getValue() > rank) {
                    if (board[cardSuit][i].getRank().getValue() - rank == 1) {
                        return new PlayerMove(cardSuit, i - 1, card, false);
                    }
                } else {
                    if (rank - board[cardSuit][i].getRank().getValue() == 1) {
                        return new PlayerMove(cardSuit, i + 1, card, false);
                    }
                }
            }

        }
        return null;
    }


    void printCardsInHand(Player player) {
        player.getCards().forEach(card -> {
            System.out.println(card.toString());
        });
    }


}
