package Models;

public class PlayerMove {
    private int x;
    private int y;
    private Card card;
    private boolean knock = false;

    public PlayerMove(int x, int y, Card card, boolean knock){
        this.x = x;
        this.y = y;
        this.card = card;
        this.knock = knock;
    }

    public PlayerMove(boolean knock){
        this.knock = knock;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public boolean isKnock() {
        return knock;
    }

    public void setKnock(boolean knock) {
        this.knock = knock;
    }
}
