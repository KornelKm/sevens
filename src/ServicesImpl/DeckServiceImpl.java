package ServicesImpl;

import Enums.Rank;
import Enums.Suit;
import Models.Card;
import Services.DeckService;

import java.util.ArrayList;
import java.util.Collections;

public class DeckServiceImpl implements DeckService {

    @Override
    public ArrayList<Card> generateNewDeck() {
        ArrayList<Card> deck = new ArrayList<>();
        for (Suit suit : Suit.values()) {
            deck.addAll(generateCardsForSuit(suit));
        }
        Collections.shuffle(deck);
        return deck;
    }

    @Override
    public ArrayList<Card> generateCardsForSuit(Suit suit) {
        ArrayList<Card> cardsOfGivenSuit = new ArrayList<>();
        for (Rank rank : Rank.values()) {
            cardsOfGivenSuit.add(new Card(suit, rank));
        }
        return cardsOfGivenSuit;
    }
}
