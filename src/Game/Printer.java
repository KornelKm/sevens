package Game;

import Models.Card;
import Models.Player;
import Models.PlayerMove;

import java.util.InputMismatchException;
import java.util.Scanner;


public class Printer {

    public void printMainMenuOptions() {
        System.out.println("Welcome in Sevens Game");
        System.out.println("Type chosen option");
        System.out.println("1 - Start Sevens Game");
        System.out.println("2 - Quit Game");
    }

    public void printBoard(Card[][] board) {
        for (Card[] row : board) {
            printRow(row);
        }
    }

    public static void printRow(Card[] row) {
        for (Card i : row) {
            if (i == null) {
                System.out.print(" ");
            } else {
                System.out.print(i.toString());
            }
            System.out.print("\t");
        }
        System.out.println();
    }


    public void  printWinner(Player player) {
        for (int i = 0; i < 5; i++) {
            System.out.println("Player " + player.getName() + " won!");
        }
        System.out.print("");
    }

    void printPlayerMove(Player player, PlayerMove playerMove) {
        if (playerMove.isKnock()) {
            System.out.println(player.getName() + " knocks(passes)");
        } else {
            System.out.println(player.getName() + " put " + playerMove.getCard().toString());
        }
    }

    int getUserIntDecision() {
        try {
            Scanner scanner = new Scanner(System.in);
            return scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("No Such Option");
        }
        return 0;
    }

    String getUserStringInput() {
        try {
            Scanner scanner = new Scanner(System.in);
            return scanner.next();
        } catch (InputMismatchException e) {
            System.out.println("Incorrect name. Try again please.");
        }
        return getUserStringInput();
    }
}
