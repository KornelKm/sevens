package Game;

import Enums.Difficulty;
import Models.*;
import ServicesImpl.DeckServiceImpl;

import java.util.*;

public class Game {
    GameState gameState = new GameState();
    Printer printer = new Printer();

    void printMainMenu() {

        printer.printMainMenuOptions();

        int mainMenuOption = printer.getUserIntDecision();

        switch (mainMenuOption) {
            case 1:
                startSevensGame();
                break;
            case 2:
                System.exit(0);
                break;
            default:
                printMainMenu();
        }

    }

    void startSevensGame() {
        prepareGame();
        startGame();
    }

    void startGame() {
        gameState.setGameInProgress(true);
        ListIterator<Player> playersIterator = gameState.getAllPlayersInOrder().listIterator();
        while (gameState.isGameInProgress()) {
            if (!playersIterator.hasNext()) {
                playersIterator = gameState.getAllPlayersInOrder().listIterator();
            }
            nextPlayerMove(playersIterator.next());

        }
    }

    public void nextPlayerMove(Player player) {
        PlayerMove playerMove = player.getNextPlayerMove(player, gameState.getBoard());
        executePlayerMove(playerMove, player);
        checkIfCurrentPlayerWon(player);
        printer.printBoard(gameState.getBoard());
    }

    public void executePlayerMove(PlayerMove playerMove, Player player) {
        if (!playerMove.isKnock()) {
            gameState.getBoard()[playerMove.getX()][playerMove.getY()] = playerMove.getCard();
            player.getCards().remove(playerMove.getCard());
        }
        printer.printPlayerMove(player, playerMove);
    }

    public void checkIfCurrentPlayerWon(Player player) {
        if (playerWon(player)) {
            printer.printWinner(player);
            gameState.setGameInProgress(false);
        }
    }

    public boolean playerWon(Player player) {
        return player.getCards().size() == 0;
    }


    void prepareGame() {
        createPlayers();
        dealTheCards(new DeckServiceImpl().generateNewDeck(), gameState.getPlayers());
        setPlayOrder();
    }

    void createPlayers() {
        gameState.setUserPlayer(createUser());
        gameState.addComputerPlayer(createComputerPlayer("Adam", Difficulty.EASY));
        gameState.addComputerPlayer(createComputerPlayer("Jason", Difficulty.HARD));
    }

    UserPlayer createUser() {
        System.out.println("Type Your name, please:");
        return  new UserPlayer(printer.getUserStringInput());
    }

    void dealTheCards(ArrayList<Card> deck, ArrayList<Player> players) {
        ListIterator<Player> playersIterator = players.listIterator();
        while (deck.size() > 0) {
            if (!playersIterator.hasNext()) {
                playersIterator = players.listIterator();
            }
            playersIterator.next().addCard(deck.remove(0));
        }
    }

    public ComputerPlayer createComputerPlayer(String name, Difficulty difficulty) {
        return new ComputerPlayer(name, difficulty);
    }

    void setPlayOrder() {
        setFirstPlayer();
        setRestOfPlayersOrder();
    }

    void setFirstPlayer() {
        gameState.getPlayers().forEach(player -> {
            if (player.hasSevenOfClubs()) {
                gameState.getAllPlayersInOrder().add(0, player);
            }
        });
    }

    void setRestOfPlayersOrder() {
        Collections.shuffle(gameState.getPlayers());
        gameState.getPlayers().forEach(player -> {
            if (!gameState.getAllPlayersInOrder().contains(player)) {
                gameState.getAllPlayersInOrder().add(player);
            }
        });
    }
}
