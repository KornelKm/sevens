package Game;

import Models.Card;
import Models.ComputerPlayer;
import Models.Player;
import Models.UserPlayer;

import java.util.ArrayList;

public class GameState {
    private UserPlayer userPlayer;
    private ArrayList<ComputerPlayer> computerPlayers = new ArrayList<>();
    private ArrayList<Player> allPlayersInOrder = new ArrayList<>();
    private boolean gameInProgress;
    private Card[][] board = new Card[4][13];


    public ArrayList<Player> getAllPlayersInOrder() {
        return allPlayersInOrder;
    }

    public void setAllPlayersInOrder(ArrayList<Player> allPlayersInOrder) {
        this.allPlayersInOrder = allPlayersInOrder;
    }


    public UserPlayer getUserPlayer() {
        return userPlayer;
    }

    public void setUserPlayer(UserPlayer userPlayer) {
        this.userPlayer = userPlayer;
    }


    public ArrayList<ComputerPlayer> getComputerPlayers() {
        return computerPlayers;
    }

    public void setComputerPlayers(ArrayList<ComputerPlayer> computerPlayers) {
        this.computerPlayers = computerPlayers;
    }

    public void addComputerPlayer(ComputerPlayer computerPlayer) {
        this.computerPlayers.add(computerPlayer);
    }

    public void addPlayer(Player player) {
        this.allPlayersInOrder.add(player);
    }

    ArrayList<Player> getPlayers() {
        ArrayList<Player> players = new ArrayList<>();
        players.add(userPlayer);
        for (ComputerPlayer computerPlayer : computerPlayers) {
            players.add(computerPlayer);
        }
        return players;
    }

    public boolean isGameInProgress() {
        return gameInProgress;
    }

    public void setGameInProgress(boolean gameInProgress) {
        this.gameInProgress = gameInProgress;
    }

    public Card[][] getBoard() {
        return board;
    }

    public void setBoard(Card[][] board) {
        this.board = board;
    }
}
