package Services;

import Models.Card;
import Models.Player;
import Models.PlayerMove;

public interface PlayerService {

     PlayerMove getNextPlayerMove(Player player, Card[][] board);
     Card getSeven();
}
