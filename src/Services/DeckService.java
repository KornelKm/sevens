package Services;

import Enums.Suit;
import Models.Card;

import java.util.ArrayList;

public interface DeckService {

    ArrayList<Card> generateNewDeck();
    ArrayList<Card> generateCardsForSuit(Suit suit);
}
