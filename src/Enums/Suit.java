package Enums;

public enum Suit {
    CLUBS(0),
    DIAMONDS(1),
    HEARTS(2),
    SPADES(3);
    private final int value;

    Suit(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static Suit getSuitByValue(int suitValue) {
        for (Suit suit : Suit.values()) {
            if (suit.value == suitValue) return suit;
        }
        throw new IllegalArgumentException("Suit not found");
    }
}
