How to run the code:
1. Pull the master.
2. Run StartGame method.

Basic implementation of CLI simulation of cards game named "Sevens".

Game enables user vs computer play. In each play there is 1 user and 2 computer players.

Game distinct 2 different difficulty modes:

Computer - Easy Mode Flow:
1. Tries to put the card with rank of seven.
2. If no seven card possessed, loops through all cards to find
first that can be put.
3. If no card can be put computer passes its move.

Computer - Hard Mode Flow:
1. First calculates the amount of cards in each suit and groups them.
2. Tries to pick and put card from the biggest to the smallest group.
3. If no card possible to put, checks if can use card with rank of seven.


What needs to be improved:
1. More test cases should be written. 